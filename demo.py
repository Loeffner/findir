from findir import FileIterator

#Pfad zum Directory mit dem gearbeitet werden soll
path = "C:/Users/andre/OneDrive/projects/matlab/captcha"


### FileIterator Objekt, das sich alle Dateien am angegebenen Pfad holt ###
fit = FileIterator(path)

print('Pfad:', fit.getPath())    # Gibt den Pfad dieses Objekts zurück
print('Offen:', fit.getOpen())   # Gibt alle geöffneten Dateien zurück

### Die Dateien ###

### Gibt eine Liste aller Dateien zurueck ###
print(fit.getFiles())

### Zum suchen in den Dateien gibt es find(), diese nimmt einen String/Liste/Tupel mit einem Pattern und sucht nach Dateien, die diesem Pattern entsprechen ###
string = '???.png'                                          # .png Dateien mit einem 3-stelligen Namen
liste = ['2018_09_??.txt', 'demo.txt', 'findir_v*.py']      # Alle Tage im September 2019, eine Datei mit dem Namen demo.py, alle Versionen von findir
tupel = ('*.m', '*.txt', '*.py')                            # Dateien mit den Endungen .m, .txt, .py
          
# print( fit.find(string)) 
# print( fit.find(liste) )
# print( fit.find(tupel) ) 


### Iterieren ###

# Öffnet eine Datei und gibt das fileobject zurück
if fit.exists('hello.txt'):                                     # Wenn die Datei hello.txt existiert
    f = fit.open('hello.txt', 'r')                              # öffnen, read-only
    print('Datei hello.txt enthält folgendes:\t\t', f.read())   # irgendwas damit machen
    fit.close('hello.txt')                                      # nicht vergessen wieder zu schließen

# Alle .txt Dateien bearbeiten
for file in fit.find('*.txt'):                                          # file entspricht dann den gefundenen .txt Dateien
    if fit.exists(file):                                                # Wenn file existiert
        f = fit.open(file, 'r')                                         # öffnen, read-only
        print('Datei', file, 'enthält folgenden Text:\t\t', f.read())   # irgendwas damit machen    
        #fit.close(file)                                                # schließen
print('\nImmer noch offen: ', fit.getOpen())        # Überprüfen, ob noch Dateien offen sind
fit.closeAll()                                      # Man kann auch alle auf einmal schließen, aber falls nicht mehrere Dateien gleichzeitig gebraucht werden, würde ich das nicht empfehlen


### Wende eine Methode auf alle Dateien an ###
def foo(file):
    return file.read()

print('\nListe der Rückgabewerte der iter() Methode: ', fit.iter(foo, pattern='*.txt', 'r'), end='\n\n')   #Wendet die Methode foo(fileobject) auf alle .txt Dateien an, die Rückgabewerte werden gesammelt in einer Liste zurück gegeben


### Neue Datei erstellen ###
fit.find('neue-datei.*')                # Gibt es eine Datei names 'neue-datei' (nein)

# Wenn eine Datei noch nicht existiert wird automatisch eine neue Datei erstellt
neu = fit.open('neue-datei.txt', 'w+')  # open a file for reading and writing
print(fit.find('neu*.txt'))             # Nach .txt-dateien suchen, die mit neu... beginnen, um zu überprüfen, ob die Datei erstellt wurde
fit.close('neue-datei.txt')             # schließen

if fit.getOpen():
    print('still a file open')          #überprüfen, ob am Ende des Programmes alle Dateien geschlossen wurden




