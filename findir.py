# Copyright (c) [2018] [Loesel Andreas]

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Version 0.3.5

import os
import fnmatch


class FileIterator():
    __dir = None        # Pfad
    __open = {}         # Dictionary der geöffneten Dateien

    def __init__(self, directory):
        if os.path.exists(directory):
            self.__dir = directory  
        else:
            raise FileNotFoundError("The system cannot find the path specified.")

    # Gibt den Pfad zurück
    def getPath(self):
        return self.__dir

    # Gibt eine Liste der geöffneten Dateien zurück.
    def getOpen(self):
        return [file for file in self.__open]

    # Gibt eine Liste aller Dateien und Unterverzeichnisse zurück.
    def getFiles(self):
        files = os.listdir(self.__dir)
        return files

    # Ermöglicht die Suche nach bestimmten Dateien. fnmatch vergleicht Dateien mit einem gegebenen Pattern.
    def find(self, pattern):
        result = []
        try:
            pattern = self.__toTuple(pattern)
        except ValueError:
            raise
        for file in self.getFiles():
            match = False
            for p in pattern:
                if fnmatch.fnmatch(file, p) and not match:
                    match = True
                    result.append(file)
        return result

    # Hilfsmethode zur Umwandlung von Listen und Strings in Tupel. Somit akzeptiert die find methode diese 3 Typen.
    def __toTuple(self, arg):      
        if isinstance(arg, (list,)):
            arg = tuple(arg)
        elif isinstance(arg, (tuple,)):
            pass
        elif isinstance(arg, (str)):
            arg = (arg,)
        else:
            raise ValueError('Illegal Argument')
        return arg

    # Öffnet eine Datei. Die offenen Dateien werden in einem Dictionary gespeichert.
    def open(self, name, mode='r', buffering=-1, encoding=None, errors=None, newline=None, closefd=True, opener=None):
        self.__open[name] = open(self.__dir + '/' + name, mode, buffering, encoding, errors, newline, closefd, opener)
        return self.__open[name]

    # Wendet die Function func auf alle Dateien an, die dem angegebenen Pattern entsprechen. Die Rückgabewerte werden gesammelt als Liste zurück gegeben.
    def iter(self, func, pattern=None, mode='r', buffering=-1, encoding=None, errors=None, newline=None, closefd=True, opener=None):
        if pattern:
            files = self.find(pattern)
        else:
            files = self.getFiles()
        retvals = []
        for file in files:
            with open(self.__dir + '/' + file, mode, buffering, encoding, errors, newline, closefd, opener) as f:
                try:
                    retvals.append(func(f))
                except TypeError:
                    raise TypeError('The function must take one positional argument: file_object')
        return retvals

    # Überprüft ob eine Datei existiert.
    def exists(self, name):
        return name in self.getFiles()        

    # Schließt eine Datei.
    def close(self, file):
        if file in self.__open:
            self.__open[file].close()
            del self.__open[file]
        return None    

    # Schließt alle Dateien
    def closeAll(self):
        for file in self.__open:
            self.__open[file].close()
        self.__open = {}
        return None

    # Sollte das Objekt zerstört werden, werden alle Dateien geschlossen.
    def __del__(self):
        self.closeAll()













    